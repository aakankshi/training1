The security of your data is very important to us. 
While data storage is not encrypted, access to your code is limited to a very small number of engineers on our 
team. 
This access is logged. Access by anyone else is controlled by Bitbucket permissions, so if your code is private, 
only you and the above engineers will be able to access it.